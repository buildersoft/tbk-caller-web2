<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmar redirect</title>
</head>
<body>
	<h2>Página antes de ir a Transbank</h2>

	<form action="${url}" method="post">
		<label>Url: <b>${url}</b></label><br> <label>Token: <b>${token}</b></label><br>
		<br> <input type="hidden" name="token_ws" value="${token}">
		<br>
		<input value="Ir a Tbk" type="submit">
	</form>
	<br>
	<a href="index">Volver</a>
	
	<hr>
	
TARJETAS DE PRUEBA CREDITO<br>
Exito (VISA): <br>
PAN:		<b>4051885600446623</b><br>
FECHA EXP: 	Cualquiera superior al dia de hoy<br>
CVV:		123<br>
Rut en banco: 11.111.111-1<br>
Clave Banco: 123<br>


<!-- 
Fracaso (MASTERCARD): 

PAN:		5186059559590568

FECHA EXP: 	Cualquiera superior al dia de hoy
CVV:		123


TARJETAS DE PRUEBA DEBITO
Exito	:	4051885600446623

Fracaso	:	5186059559590568
 -->
	
	
</body>
</html>