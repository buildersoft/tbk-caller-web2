<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fin del flujo</title>
</head>
<body>

	<c:choose>
		<c:when test="${aborted}">
			<h2>Hemos vuelto desde Transabank, pero la transaccion fue
				cancelada.</h2>
		</c:when>
		<c:otherwise>
			<h2>Felicitaciones, la transaccion de pago fue realizada con
				éxito.</h2>
		</c:otherwise>
	</c:choose>

	<br>
	<a href="index">Inicio</a>
	<hr>


</body>
</html>