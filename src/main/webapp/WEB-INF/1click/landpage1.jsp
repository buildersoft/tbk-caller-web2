<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmar redirect</title>
</head>
<body>
	<h2>Hemos vuelto desde Transbank, despues del registro</h2>

	<table>
		<tr>
			<td>errorId</td>
			<td>"<b>${errorId}</b>"
			</td>
		</tr>
		<tr>
			<td>errorDesc</td>
			<td>"<b>${errorDesc}</b>"
			</td>
		</tr>
		<tr>
			<td>authorizationCode</td>
			<td>"<b>${authorizationCode}</b>"
			</td>
		</tr>

		<tr>
			<td>cardType</td>
			<td>"<b>${cardType}</b>"
			</td>
		</tr>
		<tr>
			<td>cardNumber</td>
			<td>"<b>${cardNumber}</b>"
			</td>
		</tr>
		<tr>
			<td>responseCode</td>
			<td>"<b>${responseCode}</b>"
			</td>
		</tr>
		<tr>
			<td>tbkUser</td>
			<td>"<b>${tbkUser}</b>"
			</td>
		</tr>
	</table>

	<!-- 
	<c:if test="${letsGoTbk== true}">
		<form action="${url}" method="post">
			<br> <input type="hidden" name="token_ws" value="${token}">
			<br> <input value="Ir a Tbk, para buscar el comprobante"
				type="submit">
		</form>
	</c:if>

	<br>
	<a href="/oneclick">Inicio</a>
	<hr>
 -->

	<br>
	<a href="/oneclick">Inicio</a>
	<hr>


	<br>
	<a href="/oneclick/eliminar">Eliminar inscripción</a>
	<hr>


</body>
</html>