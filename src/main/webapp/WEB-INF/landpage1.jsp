<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Confirmar redirect</title>
</head>
<body>
	<h2>Hemos vuelto desde Transbank</h2>

	<table>
		<tr>
			<td>Resultado</td>
			<td><b>${result}</b></td>
		</tr>
		<tr>
			<td>Url Comprobante</td>
			<td><b>${url}</b></td>
		</tr>
		<tr>
			<td>Token</td>
			<td><b>${token}</b></td>
		</tr>
	</table>

	<c:if test="${letsGoTbk== true}">
		<form action="${url}" method="post">
			<br> <input type="hidden" name="token_ws" value="${token}">
			<br> <input value="Ir a Tbk, para buscar el comprobante"
				type="submit">
		</form>
	</c:if>

	<br>
	<a href="index">Inicio</a>
	<hr>


</body>
</html>