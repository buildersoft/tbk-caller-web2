package cl.buildersoft.tbkCaller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TbkCallerWeb2Application {

	public static void main(String[] args) {
		SpringApplication.run(TbkCallerWeb2Application.class, args);
	}

}
