package cl.buildersoft.tbkCaller.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import cl.buildersoft.tbkCaller.impl.TransbankCaller2;

@Controller
public class LandpageController {
	private static final Logger LOG = LogManager.getLogger(LandpageController.class);

	@PostMapping("/landpage1")
	public String land1(Map<String, Object> model, @RequestParam("token_ws") String token) {
		LOG.debug("Estoy en landpage1, Token: {}", token);
		TransbankCaller2 tc = new TransbankCaller2();

		String  result1 = tc.getTransactionResult(token);
		LOG.debug("Result: {}", result1 );
//		tc.acknowledgeTransaction(result1.getTransaction(), token);

		// TransactionResultOutput result2 = result1.getOriginalResult();

		JsonParser jp = new JsonParser();
		JsonElement j = jp.parse(result1);

		LOG.debug(j.getAsJsonObject().get("urlRedirection").getAsString());
//		LOG.debug(j.getAsJsonObject().get("url"));		
		
		
//		String url = result1.getUrlRedirection();

		model.put("url", j.getAsJsonObject().get("urlRedirection").getAsString());
		model.put("token", token);
		model.put("result", j.getAsJsonObject().get("description").getAsString());
		model.put("letsGoTbk", j.getAsJsonObject().get("vci").getAsString().equalsIgnoreCase("TSY"));
//		LOG.debug(result1.getVci());

		return "/landpage1";
	}

	@PostMapping("/landpage2")
	public String land2(Map<String, Object> model, @RequestParam(name = "token_ws", required = false) String token,
			@RequestParam(name = "TBK_TOKEN", required = false) String tbkToken) {
		LOG.debug("Estoy en landpage2. token:'{}' / TBK_TOKEN:'{}'", token, tbkToken);

		model.put("aborted", tbkToken != null);

		return "/landpage2";
	}
}
