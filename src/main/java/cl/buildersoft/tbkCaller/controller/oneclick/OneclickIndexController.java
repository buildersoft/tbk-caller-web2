package cl.buildersoft.tbkCaller.controller.oneclick;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import cl.buildersoft.tbkCaller.impl.TransbankOneClickImpl;

@Controller
public class OneclickIndexController {
	private static final String INDEX_1CLICK = "/1click/index";
	private static final Logger LOG = LogManager.getLogger(OneclickIndexController.class);
	private String token;

	@GetMapping({ "/oneclick" })
	public String oneClickController(Map<String, Object> model) {
		return INDEX_1CLICK;
	}

	@PostMapping({ "/oneclick/registro" })
	public String oneClickRegisterController(Map<String, Object> model, @RequestParam("username") String username,
			@RequestParam("mail") String mail) {
		LOG.debug("EN CONTROLLER DE ONE CLICK");

		TransbankOneClickImpl tbk = new TransbankOneClickImpl();
		String inited = null;
		try {
			inited = tbk.init2(username, mail); // , "http://localhost:8080/oneclick/landpage1");
			LOG.debug("inited: {}", inited);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}

		JsonParser jp = new JsonParser();
		JsonElement j = jp.parse(inited);

		this.token = j.getAsJsonObject().get("token").getAsString();
		String urlWebpay = j.getAsJsonObject().get("urlWebpay").getAsString();

		model.put("token", this.token);
		model.put("urlWebpay", urlWebpay);

		return "/1click/pre-send-tbk";
	}

	@PostMapping({ "/oneclick/landpage1" })
	public String landpage1Controller(Map<String, Object> model) {
		LOG.debug("THE TOKEN={}", this.token);

		TransbankOneClickImpl tbk = new TransbankOneClickImpl();

		String response = tbk.finishInscription(this.token);

		JsonParser jp = new JsonParser();
		JsonElement j = jp.parse(response);

		model.put("errorId", j.getAsJsonObject().get("errorId").getAsString());
		model.put("errorDesc", j.getAsJsonObject().get("errorDesc").getAsString());
		model.put("authorizationCode", j.getAsJsonObject().get("authorizationCode").getAsString());
		model.put("cardType", j.getAsJsonObject().get("cardType").getAsString());
		model.put("cardNumber", j.getAsJsonObject().get("cardNumber").getAsString());
		model.put("responseCode", j.getAsJsonObject().get("responseCode").getAsString());
		model.put("tbkUser", j.getAsJsonObject().get("tbkUser").getAsString());

		/**
		 * <code>
		"errorId":0,
		"errorDesc":"",
		"authorizationCode":"1213",
		"cardType":"Visa",
		"cardNumber":"XXXXXXXXXXXX6623",
		"responseCode":0,
		"tbkUser":"d8d4a013-cf39-4809-abb9-5eb7d6f859e8"
		
		 </code>
		 */

		LOG.debug("response={}", response.toString());

		return "/1click/landpage1";
	}

	@GetMapping({ "/oneclick/eliminar" })
	public String deleteOneClickController(Map<String, Object> model) {
		return "/1click/eliminar-form";
	}

	@PostMapping({ "/oneclick/desregistro" })
	public String unrecordController(Map<String, Object> model, @RequestParam("username") String username,
			@RequestParam("tbkUser") String tbkUser) {
		LOG.debug("userName:{} / tbkUser:{}", username, tbkUser);

		TransbankOneClickImpl tbk = new TransbankOneClickImpl();

		try {
			tbk.deleteInscription(username, tbkUser);
		} catch (Exception e) {
			LOG.error(e);

		}

		return INDEX_1CLICK;
	}
	
	@GetMapping({ "/oneclick/inicio-pago" })
	public String startPayOneClickController(Map<String, Object> model) {
		return "/1click/start-pay";
	}
}
