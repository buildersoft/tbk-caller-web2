package cl.buildersoft.tbkCaller.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import cl.buildersoft.tbkCaller.impl.TransbankCaller2;

@Controller
public class ComprarController {
	private static final Logger LOG = LogManager.getLogger(ComprarController.class);

	@PostMapping("/doBuy")
	public String doBuy(Map<String, Object> model, @RequestParam("amount") double amount,
			@RequestParam("sessionId") String sessionId, @RequestParam("buyOrder") String buyOrder) {
		TransbankCaller2 tbk = new TransbankCaller2();
//		Configuration config = tbk.loadConfiguration();

		LOG.info("Amount: " + ("" + amount));

		String inited = tbk.initTransaction(amount, sessionId, buyOrder);

		JsonParser jp = new JsonParser();
		JsonElement j = jp.parse(inited);

		LOG.debug(j.getAsJsonObject().get("token").getAsString());
		LOG.debug(j.getAsJsonObject().get("url").getAsString());

		model.put("token", j.getAsJsonObject().get("token").getAsString());
		model.put("url", j.getAsJsonObject().get("url").getAsString());

//		TransbankCaller tbk = new TransbankCallerImpl();
		return "/redirect1";
	}
}
