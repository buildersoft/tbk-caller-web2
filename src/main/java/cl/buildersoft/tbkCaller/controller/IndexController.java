package cl.buildersoft.tbkCaller.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

	@GetMapping({"/index", "/"})
	public String mensaje() {
//		TransbankCaller tbk = new TransbankCallerImpl();
		return "/index-page";
	}
}
